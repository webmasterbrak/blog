/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      "./resources/**/*.blade.php",
      "./resources/**/*.js",
  ],
  theme: {
    extend: {
        colors: {
            'primary': '#f6d353',
        },
    },
  },
  plugins: [],
}
