<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Blog-API

#### Se crea el index con los posts que nos sirve JSONPlaceholder, para ello se crea un objeto singleton para no instanciar varias veces la misma clase, y repositorio para separar de PostController.

#### Creo migracion, factory y modelo para popst que se usaran para servir los datos en JSON de nuetra API.

#### Creo la documentación de nuestra API.

#### Creados test unitarios

#### Instalado package jasonmccreary/laravel-test-assertions para un facil testeo de los form request al crea post.

#### He intentado hacer la mayor separación de responsabilidades posible.

#### Clonar repositorio.
~~~
git clone https://gitlab.com/webmasterbrak/blog.git
~~~

#### Crear base de datos. database: blog, user: sail y pass: password

#### Ejecutar migracion con semillas
~~~
php artisan migrate:fresh --seed
~~~
