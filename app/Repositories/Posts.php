<?php

namespace App\Repositories;

use GuzzleHttp\Exception\GuzzleException;

class Posts extends GuzzleHttpResponse
{
    /**
     * @throws GuzzleException
     */
    public function all()
    {
        return $this->get('posts');
    }

    /**
     * @throws GuzzleException
     */
    public function find($id)
    {
        return $this->get("posts/$id");
    }
}
