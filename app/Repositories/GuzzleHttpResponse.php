<?php

namespace App\Repositories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GuzzleHttpResponse
{
    protected Client $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws GuzzleException
     */
    protected function get($url)
    {
        $response = $this->client->request('GET', $url);

        return json_decode($response->getBody()->getContents());
    }
}
