<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'title'  => 'required|string|min:2|max:255|unique:posts',
            'body'   => 'string|min:10|max:500',
            'userId' => 'required|numeric'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ], 400));
    }

    public function messages(): array
    {
        return [
            'title.required'  => 'Title is required',
            'title.string'    => 'Only string is allowed for title',
            'title.min'       => 'Minimum 2 characters for title',
            'title.max'       => 'Maximum 255 characters for title',
            'title.unique'    => 'Title is already registered',
            'body.string'     => 'Only string is allowed for body',
            'body.min'        => 'Minimum 2 characters for body',
            'body.max'        => 'Maximum 255 characters for body',
            'userId.required' => 'User Id is required',
            'userId.numeric'  => 'Only numeric is allowed for User Id'
        ];
    }
}
