<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Blog-API",
 *      description="Documentación para el uso de nuestra API",
 *      x={
 *          "logo": {
 *              "url": "https://via.placeholder.com/190x90.png?text=L5-Swagger"
 *          }
 *      },
 *      @OA\Contact(
 *          email="webmasterbrak@gmail.com"
 *      ),
 *      @OA\License(
 *         name="Apache 2.0",
 *         url="https://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
class PostController extends Controller
{
    /**
     * Mostrar el listado de los posts.
     *
     * @return AnonymousResourceCollection
     *
     * @OA\Get(
     *     path="/api/posts",
     *     tags={"posts"},
     *     summary="Mostrar el listado de posts",
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar todos los posts."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function index(): AnonymousResourceCollection
    {
        return PostResource::collection(Post::latest()->paginate());
    }

    /**
     * Crear un nuevo post.
     *
     * @param StorePostRequest $request
     * @return PostResource
     * @OA\Post(
     *     path="/api/posts",
     *     tags={"posts"},
     *     summary="Crear un nuevo post",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="body",
     *                     type="string"
     *                 ),
     *                @OA\Property(
     *                     property="userId",
     *                     type="int"
     *                 ),
     *                 example={"title": "Example new post", "body": "Example new post", "userId": 2}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Creado el nuevo post.",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Error, comprueba la respuesta para ver el motivo."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="No se ha encontrado el post."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function store(StorePostRequest $request)
    {
        $post = Post::create([
            'title'  => $request->title,
            'body'   => $request->body,
            'userId' => $request->userId,
        ]);

        return new PostResource($post);
    }

    /**
     * Mostrar los datos de un post.
     *
     * @param Post $post
     * @return PostResource
     * @OA\Get(
     *     path="/api/posts/{post}",
     *     tags={"posts"},
     *     summary="Mostrar los datos de un post",
     *     @OA\Parameter(
     *         description="Parámetro necesario para la consulta de datos de un post",
     *         in="path",
     *         name="post",
     *         required=true,
     *         @OA\Schema(type="string"),
     *         @OA\Examples(example="int", value="1", summary="Introduce un número de id para el post.")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Mostrar los datos de un post."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="No se ha encontrado el post."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Post $post
     * @return void
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return void
     */
    public function destroy(Post $post)
    {
        //
    }
}
