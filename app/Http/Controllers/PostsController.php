<?php

namespace App\Http\Controllers;

use App\Repositories\Posts;
use GuzzleHttp\Exception\GuzzleException;

class PostsController
{
    protected Posts $posts;

    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @throws GuzzleException
     */
    public function index()
    {
        $posts = $this->posts->all();

        return view('blog.index', compact('posts'));
    }

    /**
     * @throws GuzzleException
     */
    public function show($id)
    {
        $post = $this->posts->find($id);

        return view('blog.show', compact('post'));
    }
}
