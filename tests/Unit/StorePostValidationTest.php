<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\PostController;
use App\Http\Requests\StorePostRequest;
use Faker\Factory;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class StorePostValidationTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_new_post()
    {
        $faker = Factory::create();
        $response = $this->postJson('/api/posts', ['title' => $faker->word(), 'body' => 'Magnico post este', 'userId' => 2]);

        $response
            ->assertStatus(201);
    }

    /** @test */
    public function store_post_form_request()
    {
        $this->assertActionUsesFormRequest(
            PostController::class,
            'store',
            StorePostRequest::class
        );
    }

    /** @test */
    public function it_verifies_validation_rules()
    {
        $formRequest = new StorePostRequest();
        $this->assertExactValidationRules([
            'title'  => ['required', 'string', 'min:2', 'max:255', 'unique:posts'],
            'body'   => ['string', 'min:10', 'max:500'],
            'userId' => ['required', 'numeric']
        ], $formRequest->rules());
    }

    /** @test */
    public function it_should_have_this_structure()
    {
        $response = $this->getJson('/api/posts/1');

        $response->assertJsonStructure([
            'data' => [
                'title',
                'body',
                'userId',
                'author' => [
                    'name',
                    'email',
                    'created_at'
                ]
            ]
        ]);
    }

    /** @test */
    public function it_must_not_have_validation_errors()
    {
        $faker = Factory::create();
        $response = $this->postJson('/api/posts', ['title' => $faker->word(), 'body' => 'Magnico post este', 'userId' => 2]);

        $response->assertValid(['title', 'body', 'userId']);
    }
}
